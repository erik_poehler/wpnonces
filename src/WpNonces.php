<?php
declare(strict_types = 1);

namespace ErikPohler\WpNonces;

final class WpNonces implements WpNoncesInterface
{
    /**
     * @var string
     */
    private $action;

    /**
     * @var string
     */
    private $name;

    public function __construct(string $action = null, string $name = null)
    {
        $this->action = $action;
        $this->name = $name;
    }

    public static function createFromActionAndName(string $action, string $name) : WpNonces
    {
        return new self($action, $name);
    }
}