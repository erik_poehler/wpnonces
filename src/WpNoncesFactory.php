<?php
declare(strict_types = 1);

namespace ErikPohler\WpNonces;

final class WpNoncesFactory
{
    public static function createFromActionAndName(string $action = null, string $name = null) : WpNoncesInterface
    {
        return new WpNonces($action, $name);
    }
}
