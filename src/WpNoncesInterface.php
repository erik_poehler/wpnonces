<?php
declare(strict_types = 1);

namespace ErikPohler\WpNonces;

interface WpNoncesInterface
{
    public function __construct(string $action = null, string $name = null);
    
    public static function createFromActionAndName(string $action = null, string $name = null) : WpNonces;
}
