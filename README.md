# WpNonces
WpNonces is a small wrapper package for WordPress' nonce functions.

## Running PHP Code Sniffer
PHP Code Sniffer Rules can be found in phpcs.xml.
Test the package for PHP Code Sniffer compliance: `vendor/bin/phpcs`
Autofix errors: `vendor/bin/phpcbf`

## Running Code Quality Analysis PHPMD
PHPMD rules can be found in phpmd.xml
`vendor/bin/phpmd ./src text codesize,cleancode,controversial,design,naming,unusedcode ./phpmd.xml`

